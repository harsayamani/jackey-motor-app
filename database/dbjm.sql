-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 21, 2021 at 10:39 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbjm`
--

-- --------------------------------------------------------

--
-- Table structure for table `car`
--

CREATE TABLE `car` (
  `car_id` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL,
  `variant` varchar(255) NOT NULL,
  `merk` varchar(255) NOT NULL,
  `prod_year` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car`
--

INSERT INTO `car` (`car_id`, `model`, `variant`, `merk`, `prod_year`) VALUES
('CAR-g1oARr', 'Kijang Grand Extra', 'Blue', 'Toyota', ' 2029'),
('car0Yg0Wq', 'swift', 'black', 'toyota', '2005');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `cust_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `address` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`cust_id`, `name`, `phone_number`, `address`) VALUES
('CUST-FSrzrC', 'Harsa Yamani', '08977025416', 'Cirebons'),
('cust-twpHor', 'Harsa Yamani', '08977025416', 'Cirebon');

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `invoice_id` varchar(255) NOT NULL,
  `car_id` varchar(25) NOT NULL,
  `cust_id` varchar(255) NOT NULL,
  `chasis` varchar(255) NOT NULL,
  `received` varchar(255) NOT NULL,
  `reg_date` date NOT NULL,
  `date_in` date NOT NULL,
  `date_out` date NOT NULL,
  `engine` varchar(255) NOT NULL,
  `km` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`invoice_id`, `car_id`, `cust_id`, `chasis`, `received`, `reg_date`, `date_in`, `date_out`, `engine`, `km`) VALUES
('INV-H8zZ89N72vdc', 'CAR-g1oARr', 'CUST-FSrzrC', '12345678', 'E4223BE', '2021-07-09', '2021-07-09', '2021-07-09', '12345678', 120),
('INV-t8KGQPHAXJNr', 'CAR-g1oARr', 'CUST-FSrzrC', '22132', 'E3434BG', '2021-07-01', '2021-07-01', '2021-07-01', '234324', 2323),
('invWZC0q2jjPx2i', 'car0Yg0Wq', 'cust-twpHor', 'dasdsad', 'dafaf', '2021-06-09', '2021-06-09', '2021-06-09', 'dsafa', 2000);

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `item_id` varchar(255) NOT NULL,
  `service_id` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `invoice_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `itemmechanic`
--

CREATE TABLE `itemmechanic` (
  `item_id` text COLLATE utf8_bin NOT NULL,
  `service_id` text COLLATE utf8_bin DEFAULT NULL,
  `qty` int(32) DEFAULT NULL,
  `price` int(32) DEFAULT NULL,
  `amount` int(32) DEFAULT NULL,
  `invoice_id` text COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `itemmechanic`
--

INSERT INTO `itemmechanic` (`item_id`, `service_id`, `qty`, `price`, `amount`, `invoice_id`) VALUES
('ITEM-2SMhdk', 'SS-70xKK3', 1, 120000, 120000, 'INV-BqANVQi8YVmn'),
('ITEM-f2wL8J', 'SS-7ytZHS', 1, 50000, 50000, 'INV-BqANVQi8YVmn');

-- --------------------------------------------------------

--
-- Table structure for table `mechanic`
--

CREATE TABLE `mechanic` (
  `invoice_id` text COLLATE utf8_bin NOT NULL,
  `cust_id` text COLLATE utf8_bin DEFAULT NULL,
  `car_id` text COLLATE utf8_bin DEFAULT NULL,
  `chasis` text COLLATE utf8_bin DEFAULT NULL,
  `received` text COLLATE utf8_bin DEFAULT NULL,
  `reg_date` date DEFAULT NULL,
  `date_in` date DEFAULT NULL,
  `date_out` date DEFAULT NULL,
  `engine` text COLLATE utf8_bin DEFAULT NULL,
  `km` int(32) DEFAULT NULL,
  `mechanic` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `mechanic`
--

INSERT INTO `mechanic` (`invoice_id`, `cust_id`, `car_id`, `chasis`, `received`, `reg_date`, `date_in`, `date_out`, `engine`, `km`, `mechanic`) VALUES
('INV-BqANVQi8YVmn', 'CUST-FSrzrC', 'CAR-g1oARr', 'TEST', 'TEST', '2021-07-14', '2021-07-14', '2021-07-14', 'TEST', 123456, 'Salman');

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `service_id` varchar(255) NOT NULL,
  `serv_supl` varchar(255) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`service_id`, `serv_supl`, `price`) VALUES
('SS-70xKK3', 'Ganti Oli', 120000),
('SS-7ytZHS', 'Service', 50000),
('SS-kyjZrW', 'Bubut', 50000);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `password`, `name`) VALUES
('user1', 'cad4f618209a16b3bb', 'User1'),
('user2', 'c2def21a209812b9', 'User1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `car`
--
ALTER TABLE `car`
  ADD PRIMARY KEY (`car_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`cust_id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`invoice_id`),
  ADD KEY `car_id` (`car_id`),
  ADD KEY `cust_id` (`cust_id`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`item_id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `itemmechanic`
--
ALTER TABLE `itemmechanic`
  ADD PRIMARY KEY (`item_id`(255));

--
-- Indexes for table `mechanic`
--
ALTER TABLE `mechanic`
  ADD PRIMARY KEY (`invoice_id`(255));

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `invoice`
--
ALTER TABLE `invoice`
  ADD CONSTRAINT `car_id` FOREIGN KEY (`car_id`) REFERENCES `car` (`car_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `cust_id` FOREIGN KEY (`cust_id`) REFERENCES `customer` (`cust_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `invoice_ibfk_1` FOREIGN KEY (`cust_id`) REFERENCES `customer` (`cust_id`),
  ADD CONSTRAINT `invoice_ibfk_2` FOREIGN KEY (`car_id`) REFERENCES `car` (`car_id`);

--
-- Constraints for table `item`
--
ALTER TABLE `item`
  ADD CONSTRAINT `service_id` FOREIGN KEY (`service_id`) REFERENCES `service` (`service_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
