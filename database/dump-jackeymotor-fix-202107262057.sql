PGDMP     5    9                y            jackeymotor    13.3    13.3 "    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    33166    jackeymotor    DATABASE     n   CREATE DATABASE jackeymotor WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'Indonesian_Indonesia.1252';
    DROP DATABASE jackeymotor;
                postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
                postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                   postgres    false    3            �            1259    33240    car    TABLE     {   CREATE TABLE public.car (
    car_id text NOT NULL,
    model text,
    variant text,
    prod_year text,
    merk text
);
    DROP TABLE public.car;
       public         heap    postgres    false    3            �            1259    33246    customer    TABLE     t   CREATE TABLE public.customer (
    cust_id text NOT NULL,
    name text,
    phone_number text,
    address text
);
    DROP TABLE public.customer;
       public         heap    postgres    false    3            �            1259    33252    invoice    TABLE     �   CREATE TABLE public.invoice (
    invoice_id text NOT NULL,
    cust_id text,
    car_id text,
    chasis text,
    received text,
    reg_date date,
    date_in date,
    date_out date,
    engine text,
    km integer
);
    DROP TABLE public.invoice;
       public         heap    postgres    false    3            �            1259    33258    item    TABLE     �   CREATE TABLE public.item (
    item_id text NOT NULL,
    service_id text,
    qty integer,
    price integer,
    amount integer,
    invoice_id text
);
    DROP TABLE public.item;
       public         heap    postgres    false    3            �            1259    33316    itemmechanic    TABLE     �   CREATE TABLE public.itemmechanic (
    item_id text NOT NULL,
    service_id text,
    qty integer,
    price integer,
    amount integer,
    invoice_id text
);
     DROP TABLE public.itemmechanic;
       public         heap    postgres    false    3            �            1259    33308    mechanic    TABLE       CREATE TABLE public.mechanic (
    invoice_id text NOT NULL,
    cust_id text,
    car_id text,
    chasis text,
    received text,
    reg_date date,
    date_in date,
    date_out date,
    engine text,
    km integer,
    mechanic character varying NOT NULL
);
    DROP TABLE public.mechanic;
       public         heap    postgres    false    3            �            1259    33264    service    TABLE     e   CREATE TABLE public.service (
    service_id text NOT NULL,
    serv_supl text,
    price integer
);
    DROP TABLE public.service;
       public         heap    postgres    false    3            �            1259    33270    users    TABLE     \   CREATE TABLE public.users (
    username text NOT NULL,
    password text,
    name text
);
    DROP TABLE public.users;
       public         heap    postgres    false    3            �          0    33240    car 
   TABLE DATA           F   COPY public.car (car_id, model, variant, prod_year, merk) FROM stdin;
    public          postgres    false    200            �          0    33246    customer 
   TABLE DATA           H   COPY public.customer (cust_id, name, phone_number, address) FROM stdin;
    public          postgres    false    201            �          0    33252    invoice 
   TABLE DATA           y   COPY public.invoice (invoice_id, cust_id, car_id, chasis, received, reg_date, date_in, date_out, engine, km) FROM stdin;
    public          postgres    false    202            �          0    33258    item 
   TABLE DATA           S   COPY public.item (item_id, service_id, qty, price, amount, invoice_id) FROM stdin;
    public          postgres    false    203            �          0    33316    itemmechanic 
   TABLE DATA           [   COPY public.itemmechanic (item_id, service_id, qty, price, amount, invoice_id) FROM stdin;
    public          postgres    false    207            �          0    33308    mechanic 
   TABLE DATA           �   COPY public.mechanic (invoice_id, cust_id, car_id, chasis, received, reg_date, date_in, date_out, engine, km, mechanic) FROM stdin;
    public          postgres    false    206            �          0    33264    service 
   TABLE DATA           ?   COPY public.service (service_id, serv_supl, price) FROM stdin;
    public          postgres    false    204            �          0    33270    users 
   TABLE DATA           9   COPY public.users (username, password, name) FROM stdin;
    public          postgres    false    205            E           2606    33277    car car_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.car
    ADD CONSTRAINT car_pkey PRIMARY KEY (car_id);
 6   ALTER TABLE ONLY public.car DROP CONSTRAINT car_pkey;
       public            postgres    false    200            G           2606    33279    customer customer_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public.customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (cust_id);
 @   ALTER TABLE ONLY public.customer DROP CONSTRAINT customer_pkey;
       public            postgres    false    201            I           2606    33281    invoice invoice_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.invoice
    ADD CONSTRAINT invoice_pkey PRIMARY KEY (invoice_id);
 >   ALTER TABLE ONLY public.invoice DROP CONSTRAINT invoice_pkey;
       public            postgres    false    202            Q           2606    33315    mechanic invoice_pkey_1 
   CONSTRAINT     ]   ALTER TABLE ONLY public.mechanic
    ADD CONSTRAINT invoice_pkey_1 PRIMARY KEY (invoice_id);
 A   ALTER TABLE ONLY public.mechanic DROP CONSTRAINT invoice_pkey_1;
       public            postgres    false    206            K           2606    33283    item item_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY public.item
    ADD CONSTRAINT item_pkey PRIMARY KEY (item_id);
 8   ALTER TABLE ONLY public.item DROP CONSTRAINT item_pkey;
       public            postgres    false    203            S           2606    33323    itemmechanic item_pkey_1 
   CONSTRAINT     [   ALTER TABLE ONLY public.itemmechanic
    ADD CONSTRAINT item_pkey_1 PRIMARY KEY (item_id);
 B   ALTER TABLE ONLY public.itemmechanic DROP CONSTRAINT item_pkey_1;
       public            postgres    false    207            M           2606    33285    service service_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.service
    ADD CONSTRAINT service_pkey PRIMARY KEY (service_id);
 >   ALTER TABLE ONLY public.service DROP CONSTRAINT service_pkey;
       public            postgres    false    204            O           2606    33287    users users_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (username);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public            postgres    false    205            T           2606    33288    invoice car_id    FK CONSTRAINT     n   ALTER TABLE ONLY public.invoice
    ADD CONSTRAINT car_id FOREIGN KEY (car_id) REFERENCES public.car(car_id);
 8   ALTER TABLE ONLY public.invoice DROP CONSTRAINT car_id;
       public          postgres    false    2885    202    200            U           2606    33293    invoice cust_id    FK CONSTRAINT     v   ALTER TABLE ONLY public.invoice
    ADD CONSTRAINT cust_id FOREIGN KEY (cust_id) REFERENCES public.customer(cust_id);
 9   ALTER TABLE ONLY public.invoice DROP CONSTRAINT cust_id;
       public          postgres    false    202    201    2887            V           2606    33298    item invoice_id    FK CONSTRAINT     {   ALTER TABLE ONLY public.item
    ADD CONSTRAINT invoice_id FOREIGN KEY (invoice_id) REFERENCES public.invoice(invoice_id);
 9   ALTER TABLE ONLY public.item DROP CONSTRAINT invoice_id;
       public          postgres    false    2889    203    202            W           2606    33303    item service_id    FK CONSTRAINT     {   ALTER TABLE ONLY public.item
    ADD CONSTRAINT service_id FOREIGN KEY (service_id) REFERENCES public.service(service_id);
 9   ALTER TABLE ONLY public.item DROP CONSTRAINT service_id;
       public          postgres    false    2893    204    203            �   =   x�sv�M7�w*����J�KWp/J�KQp�()J�t�)M�T020��ɯ�/I����� �1_      �   ;   x�s�u.�*r��H,*NT�L�M���4��47702514�t�,JM��+����� ��X      �   �   x�u��
�0��?�I�&��*m���)8
A��f�)�}��}C�ek���q�p�Nr;�%
~�O��c&6�u�m$(h��TM%��y�}w<���K��4�"6m��5,E�`q[	!�
^.      �   {   x��q��-3�
��ͮ̊*
�4�45 NC����aQea�gnT����	���W��e^Y��i�e�G�a�y��;~]%���^~E]NΡN��.4�i37Ů/F��� i8D      �   U   x��q��M3*�����5�,���4�45 (����T���i����	�d웑��dP��m�dhV�0���qqq ea
      �   V   x����u*t�̴����t�u.�*r�tv�M7�w*�q�FF��溆&8�`U�F�&�f���9��y\1z\\\ ��      �   I   x��5�,���N-*�LN�45 ��`��ʬ��pN�Ҥ����A���1�{b^I��N&��X*F��� x��      �   *   x�+-N-2�LNL1I33�02�L44K2NJ�Ip��qqq �H	�      "    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    33166    jackeymotor    DATABASE     n   CREATE DATABASE jackeymotor WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'Indonesian_Indonesia.1252';
    DROP DATABASE jackeymotor;
                postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
                postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                   postgres    false    3            �            1259    33240    car    TABLE     {   CREATE TABLE public.car (
    car_id text NOT NULL,
    model text,
    variant text,
    prod_year text,
    merk text
);
    DROP TABLE public.car;
       public         heap    postgres    false    3            �            1259    33246    customer    TABLE     t   CREATE TABLE public.customer (
    cust_id text NOT NULL,
    name text,
    phone_number text,
    address text
);
    DROP TABLE public.customer;
       public         heap    postgres    false    3            �            1259    33252    invoice    TABLE     �   CREATE TABLE public.invoice (
    invoice_id text NOT NULL,
    cust_id text,
    car_id text,
    chasis text,
    received text,
    reg_date date,
    date_in date,
    date_out date,
    engine text,
    km integer
);
    DROP TABLE public.invoice;
       public         heap    postgres    false    3            �            1259    33258    item    TABLE     �   CREATE TABLE public.item (
    item_id text NOT NULL,
    service_id text,
    qty integer,
    price integer,
    amount integer,
    invoice_id text
);
    DROP TABLE public.item;
       public         heap    postgres    false    3            �            1259    33316    itemmechanic    TABLE     �   CREATE TABLE public.itemmechanic (
    item_id text NOT NULL,
    service_id text,
    qty integer,
    price integer,
    amount integer,
    invoice_id text
);
     DROP TABLE public.itemmechanic;
       public         heap    postgres    false    3            �            1259    33308    mechanic    TABLE       CREATE TABLE public.mechanic (
    invoice_id text NOT NULL,
    cust_id text,
    car_id text,
    chasis text,
    received text,
    reg_date date,
    date_in date,
    date_out date,
    engine text,
    km integer,
    mechanic character varying NOT NULL
);
    DROP TABLE public.mechanic;
       public         heap    postgres    false    3            �            1259    33264    service    TABLE     e   CREATE TABLE public.service (
    service_id text NOT NULL,
    serv_supl text,
    price integer
);
    DROP TABLE public.service;
       public         heap    postgres    false    3            �            1259    33270    users    TABLE     \   CREATE TABLE public.users (
    username text NOT NULL,
    password text,
    name text
);
    DROP TABLE public.users;
       public         heap    postgres    false    3            �          0    33240    car 
   TABLE DATA           F   COPY public.car (car_id, model, variant, prod_year, merk) FROM stdin;
    public          postgres    false    200   n       �          0    33246    customer 
   TABLE DATA           H   COPY public.customer (cust_id, name, phone_number, address) FROM stdin;
    public          postgres    false    201   G        �          0    33252    invoice 
   TABLE DATA           y   COPY public.invoice (invoice_id, cust_id, car_id, chasis, received, reg_date, date_in, date_out, engine, km) FROM stdin;
    public          postgres    false    202   E        �          0    33258    item 
   TABLE DATA           S   COPY public.item (item_id, service_id, qty, price, amount, invoice_id) FROM stdin;
    public          postgres    false    203   �        �          0    33316    itemmechanic 
   TABLE DATA           [   COPY public.itemmechanic (item_id, service_id, qty, price, amount, invoice_id) FROM stdin;
    public          postgres    false    207   �        �          0    33308    mechanic 
   TABLE DATA           �   COPY public.mechanic (invoice_id, cust_id, car_id, chasis, received, reg_date, date_in, date_out, engine, km, mechanic) FROM stdin;
    public          postgres    false    206   _        �          0    33264    service 
   TABLE DATA           ?   COPY public.service (service_id, serv_supl, price) FROM stdin;
    public          postgres    false    204   `        �          0    33270    users 
   TABLE DATA           9   COPY public.users (username, password, name) FROM stdin;
    public          postgres    false    205   S        E           2606    33277    car car_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.car
    ADD CONSTRAINT car_pkey PRIMARY KEY (car_id);
 6   ALTER TABLE ONLY public.car DROP CONSTRAINT car_pkey;
       public            postgres    false    200            G           2606    33279    customer customer_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public.customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (cust_id);
 @   ALTER TABLE ONLY public.customer DROP CONSTRAINT customer_pkey;
       public            postgres    false    201            I           2606    33281    invoice invoice_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.invoice
    ADD CONSTRAINT invoice_pkey PRIMARY KEY (invoice_id);
 >   ALTER TABLE ONLY public.invoice DROP CONSTRAINT invoice_pkey;
       public            postgres    false    202            Q           2606    33315    mechanic invoice_pkey_1 
   CONSTRAINT     ]   ALTER TABLE ONLY public.mechanic
    ADD CONSTRAINT invoice_pkey_1 PRIMARY KEY (invoice_id);
 A   ALTER TABLE ONLY public.mechanic DROP CONSTRAINT invoice_pkey_1;
       public            postgres    false    206            K           2606    33283    item item_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY public.item
    ADD CONSTRAINT item_pkey PRIMARY KEY (item_id);
 8   ALTER TABLE ONLY public.item DROP CONSTRAINT item_pkey;
       public            postgres    false    203            S           2606    33323    itemmechanic item_pkey_1 
   CONSTRAINT     [   ALTER TABLE ONLY public.itemmechanic
    ADD CONSTRAINT item_pkey_1 PRIMARY KEY (item_id);
 B   ALTER TABLE ONLY public.itemmechanic DROP CONSTRAINT item_pkey_1;
       public            postgres    false    207            M           2606    33285    service service_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.service
    ADD CONSTRAINT service_pkey PRIMARY KEY (service_id);
 >   ALTER TABLE ONLY public.service DROP CONSTRAINT service_pkey;
       public            postgres    false    204            O           2606    33287    users users_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (username);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public            postgres    false    205            T           2606    33288    invoice car_id    FK CONSTRAINT     n   ALTER TABLE ONLY public.invoice
    ADD CONSTRAINT car_id FOREIGN KEY (car_id) REFERENCES public.car(car_id);
 8   ALTER TABLE ONLY public.invoice DROP CONSTRAINT car_id;
       public          postgres    false    2885    202    200            U           2606    33293    invoice cust_id    FK CONSTRAINT     v   ALTER TABLE ONLY public.invoice
    ADD CONSTRAINT cust_id FOREIGN KEY (cust_id) REFERENCES public.customer(cust_id);
 9   ALTER TABLE ONLY public.invoice DROP CONSTRAINT cust_id;
       public          postgres    false    202    201    2887            V           2606    33298    item invoice_id    FK CONSTRAINT     {   ALTER TABLE ONLY public.item
    ADD CONSTRAINT invoice_id FOREIGN KEY (invoice_id) REFERENCES public.invoice(invoice_id);
 9   ALTER TABLE ONLY public.item DROP CONSTRAINT invoice_id;
       public          postgres    false    2889    203    202            W           2606    33303    item service_id    FK CONSTRAINT     {   ALTER TABLE ONLY public.item
    ADD CONSTRAINT service_id FOREIGN KEY (service_id) REFERENCES public.service(service_id);
 9   ALTER TABLE ONLY public.item DROP CONSTRAINT service_id;
       public          postgres    false    2893    204    203           