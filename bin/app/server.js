const restify = require('restify');
const corsMiddleware = require('restify-cors-middleware');
const project = require('../../package.json');
const basicAuth = require('../auth/basic_auth_helper');
const jwtAuth = require('../auth/jwt_auth_helper');
const wrapper = require('../helpers/utils/wrapper');
const userHandler = require('../modules/user/handlers/api_handler');
const carHandler = require('../modules/car/handlers/api_handler');
const customerHandler = require('../modules/customer/handlers/api_handler');
const invoiceHandler = require('../modules/invoice/handlers/api_handler');
const mechanicHandler = require('../modules/mechanic/handlers/api_handler');
const serviceHandler = require('../modules/service/handlers/api_handler');
const itemHandler = require('../modules/item/handlers/api_handler');
const itemMechanicHandler = require('../modules/itemmechanic/handlers/api_handler');

function AppServer() {
  this.server = restify.createServer({
    name: `${project.name}-server`,
    version: project.version,
  });

  this.server.serverKey = '';
  this.server.use(restify.plugins.acceptParser(this.server.acceptable));
  this.server.use(restify.plugins.queryParser());
  this.server.use(restify.plugins.bodyParser());
  this.server.use(restify.plugins.authorizationParser());

  // required for CORS configuration
  const corsConfig = corsMiddleware({
    preflightMaxAge: 5,
    origins: ['*'],
    // ['*'] -> to expose all header, any type header will be allow to access
    // X-Requested-With,content-type,GET, POST, PUT, PATCH, DELETE, OPTIONS -> header type
    allowHeaders: ['Authorization'],
    exposeHeaders: ['Authorization'],
  });
  this.server.pre(corsConfig.preflight);
  this.server.use(corsConfig.actual);

  // // required for basic auth
  this.server.use(basicAuth.init());

  // anonymous can access the end point, place code bellow
  this.server.get('/', (req, res) => {
    wrapper.response(
      res,
      'success',
      wrapper.data('Index'),
      'This service is running properly',
    );
  });

  // User
  this.server.post(
    '/api/v1/user',
    userHandler.registerUser,
  );
  this.server.post(
    '/api/v1/user/login',
    userHandler.loginUser,
  );
  this.server.put(
    '/api/v1/user/:username',
    jwtAuth.verifyToken,
    userHandler.updateUser,
  );
  this.server.del(
    '/api/v1/user/:username',
    jwtAuth.verifyToken,
    carHandler.deleteCar,
  );
  this.server.get(
    '/api/v1/user',
    jwtAuth.verifyToken,
    userHandler.getAllUser,
  );
  this.server.get(
    '/api/v1/user/:username',
    jwtAuth.verifyToken,
    userHandler.getUser,
  );

  // Car
  this.server.post(
    '/api/v1/car',
    jwtAuth.verifyToken,
    carHandler.createCar,
  );
  this.server.put(
    '/api/v1/car/:carId',
    jwtAuth.verifyToken,
    carHandler.updateCar,
  );
  this.server.del(
    '/api/v1/car/:carId',
    jwtAuth.verifyToken,
    carHandler.deleteCar,
  );
  this.server.get(
    '/api/v1/car',
    jwtAuth.verifyToken,
    carHandler.getAllCar,
  );
  this.server.get(
    '/api/v1/car/:carId',
    jwtAuth.verifyToken,
    carHandler.getCar,
  );

  // Customer
  this.server.post(
    '/api/v1/customer',
    jwtAuth.verifyToken,
    customerHandler.createCustomer,
  );
  this.server.put(
    '/api/v1/customer/:custId',
    jwtAuth.verifyToken,
    customerHandler.updateCustomer,
  );
  this.server.del(
    '/api/v1/customer/:custId',
    jwtAuth.verifyToken,
    customerHandler.deleteCustomer,
  );
  this.server.get(
    '/api/v1/customer',
    jwtAuth.verifyToken,
    customerHandler.getAllCustomer,
  );
  this.server.get(
    '/api/v1/customer/:custId',
    jwtAuth.verifyToken,
    customerHandler.getCustomer,
  );

  // Invoice
  this.server.post(
    '/api/v1/invoice',
    jwtAuth.verifyToken,
    invoiceHandler.createInvoice,
  );
  this.server.put(
    '/api/v1/invoice/:invoiceId',
    jwtAuth.verifyToken,
    invoiceHandler.updateInvoice,
  );
  this.server.del(
    '/api/v1/invoice/:invoiceId',
    jwtAuth.verifyToken,
    invoiceHandler.deleteInvoice,
  );
  this.server.get(
    '/api/v1/invoice',
    jwtAuth.verifyToken,
    invoiceHandler.getAllInvoice,
  );
  this.server.get(
    '/api/v1/invoice/:invoiceId',
    jwtAuth.verifyToken,
    invoiceHandler.getInvoice,
  );

  // Mechanic
  this.server.post(
    '/api/v1/mechanic',
    jwtAuth.verifyToken,
    mechanicHandler.createInvoice,
  );
  this.server.put(
    '/api/v1/mechanic/:invoiceId',
    jwtAuth.verifyToken,
    mechanicHandler.updateInvoice,
  );
  this.server.del(
    '/api/v1/mechanic/:invoiceId',
    jwtAuth.verifyToken,
    mechanicHandler.deleteInvoice,
  );
  this.server.get(
    '/api/v1/mechanic',
    jwtAuth.verifyToken,
    mechanicHandler.getAllInvoice,
  );
  this.server.get(
    '/api/v1/mechanic/:invoiceId',
    jwtAuth.verifyToken,
    mechanicHandler.getInvoice,
  );

  // Service
  this.server.post(
    '/api/v1/service',
    jwtAuth.verifyToken,
    serviceHandler.createService,
  );
  this.server.put(
    '/api/v1/service/:serviceId',
    jwtAuth.verifyToken,
    serviceHandler.updateService,
  );
  this.server.del(
    '/api/v1/service/:serviceId',
    jwtAuth.verifyToken,
    serviceHandler.deleteService,
  );
  this.server.get(
    '/api/v1/service',
    jwtAuth.verifyToken,
    serviceHandler.getAllService,
  );
  this.server.get(
    '/api/v1/service/:serviceId',
    jwtAuth.verifyToken,
    serviceHandler.getService,
  );

  // Item
  this.server.post(
    '/api/v1/item',
    jwtAuth.verifyToken,
    itemHandler.createItem,
  );
  this.server.put(
    '/api/v1/item/:itemId',
    jwtAuth.verifyToken,
    itemHandler.updateItem,
  );
  this.server.del(
    '/api/v1/item/:itemId',
    jwtAuth.verifyToken,
    itemHandler.deleteItem,
  );
  this.server.get(
    '/api/v1/item',
    jwtAuth.verifyToken,
    itemHandler.getAllItem,
  );
  this.server.get(
    '/api/v1/item/:itemId',
    jwtAuth.verifyToken,
    itemHandler.getItem,
  );
  // Item Mechanic
  this.server.post(
    '/api/v1/item-mechanic',
    jwtAuth.verifyToken,
    itemMechanicHandler.createItem,
  );
  this.server.put(
    '/api/v1/item-mechanic/:itemId',
    jwtAuth.verifyToken,
    itemMechanicHandler.updateItem,
  );
  this.server.del(
    '/api/v1/item-mechanic/:itemId',
    jwtAuth.verifyToken,
    itemMechanicHandler.deleteItem,
  );
  this.server.get(
    '/api/v1/item-mechanic',
    jwtAuth.verifyToken,
    itemMechanicHandler.getAllItem,
  );
  this.server.get(
    '/api/v1/item-mechanic/:itemId',
    jwtAuth.verifyToken,
    itemMechanicHandler.getItem,
  );
}

module.exports = AppServer;
