const wrapper = require('../../../helpers/utils/wrapper');
const commandHandler = require('../repositories/commands/command_handler');
const commandModel = require('../repositories/commands/command_model');
const queryHandler = require('../repositories/queries/query_handler');
const validator = require('../utils/validator');
const {
  ERROR: httpError,
  SUCCESS: http,
} = require('../../../helpers/http-status/status_code');

const loginUser = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(
    payload,
    commandModel.login,
  );
  const postRequest = async (result) => {
    if (result.err) {
      return result;
    }
    return commandHandler.loginUser(result.data);
  };
  const sendResponse = async (result) => {
    /* eslint no-unused-expressions: [2, { allowTernary: true }] */
    result.err
      ? wrapper.response(
        res,
        'fail',
        result,
        'Login User Fail',
        httpError.BAD_REQUEST,
      )
      : wrapper.response(res, 'success', result, 'Login User Success', http.OK);
  };
  sendResponse(await postRequest(validatePayload));
};

const registerUser = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(
    payload,
    commandModel.registerUser,
  );
  const postRequest = async (result) => {
    if (result.err) {
      return result;
    }
    return commandHandler.registerUser(result.data);
  };
  const sendResponse = async (result) => {
    /* eslint no-unused-expressions: [2, { allowTernary: true }] */
    result.err
      ? wrapper.response(
        res,
        'fail',
        result,
        'Register User Fail',
        httpError.BAD_REQUEST,
      )
      : wrapper.response(res, 'success', result, 'Register User Success', http.OK);
  };
  sendResponse(await postRequest(validatePayload));
};

const updateUser = async (req, res) => {
  const { username } = req.params;

  const payload = req.body;
  const validatePayload = validator.isValidPayload(
    payload,
    commandModel.updateUser,
  );
  const postRequest = async (result) => {
    if (result.err) {
      return result;
    }
    return commandHandler.updateUser(username, result.data);
  };
  const sendResponse = async (result) => {
    /* eslint no-unused-expressions: [2, { allowTernary: true }] */
    result.err
      ? wrapper.response(
        res,
        'fail',
        result,
        'Update User Fail',
        httpError.BAD_REQUEST,
      )
      : wrapper.response(res, 'success', result, 'Update User Success', http.OK);
  };
  sendResponse(await postRequest(validatePayload));
};

const deleteUser = async (req, res) => {
  const { username } = req.params;

  const postRequest = async () => commandHandler.deleteUser(username);
  const sendResponse = async (result) => {
    /* eslint no-unused-expressions: [2, { allowTernary: true }] */
    result.err
      ? wrapper.response(
        res,
        'fail',
        result,
        'Delete User Fail',
        httpError.NOT_FOUND,
      )
      : wrapper.response(res, 'success', result, 'Delete User Success', http.OK);
  };
  sendResponse(await postRequest());
};

const getUser = async (req, res) => {
  const { username } = req.params;

  const getData = async () => queryHandler.getUser(username);
  const sendResponse = async (result) => {
    result.err
      ? wrapper.response(res, 'fail', result, 'Get User Fail', httpError.NOT_FOUND)
      : wrapper.response(res, 'success', result, 'Get User Success', http.OK);
  };
  sendResponse(await getData());
};

const getAllUser = async (req, res) => {
//   const { testId } = req.query;
  const getData = async () => queryHandler.getAllUser();
  const sendResponse = async (result) => {
    result.err
      ? wrapper.response(
        res,
        'fail',
        result,
        'Get All User Fail',
        httpError.NOT_FOUND,
      )
      : wrapper.response(res, 'success', result, 'Get All User Success', http.OK);
  };
  sendResponse(await getData());
};

module.exports = {
  registerUser,
  loginUser,
  updateUser,
  deleteUser,
  getUser,
  getAllUser
};
