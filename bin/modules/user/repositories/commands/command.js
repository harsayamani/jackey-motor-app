class Command {
  constructor(db) {
    this.db = db;
  }

  async insertUser(statement) {
    const result = await this.db.query(statement);
    return result;
  }

  async updateUser(statement) {
    const result = await this.db.query(statement);
    return result;
  }

  async deleteUser(statement) {
    const result = await this.db.query(statement);
    return result;
  }
}

module.exports = Command;

