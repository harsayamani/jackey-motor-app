const joi = require('joi');

const registerUser = joi.object({
  username: joi.string().required(),
  password: joi.string().required(),
  name: joi.string().required(),
});

const updateUser = joi.object({
  password: joi.string(),
  name: joi.string(),
});

const login = joi.object({
  username: joi.string().required(),
  password: joi.string().required(),
});

module.exports = {
  registerUser,
  updateUser,
  login
};
