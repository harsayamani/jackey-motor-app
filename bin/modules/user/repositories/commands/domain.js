const Query = require('../queries/query');
const Command = require('./command');
const wrapper = require('../../../../helpers/utils/wrapper');
const commonUtil = require('../../../../helpers/utils/common');
const { NotFoundError, BadRequestError, UnauthorizedError } = require('../../../../helpers/error');
const algorithm = 'aes-256-ctr';
const secretKey = 'Dom@in2018';
const jwtAuth = require('../../../../auth/jwt_auth_helper');

class User {
  constructor(db) {
    this.command = new Command(db);
    this.query = new Query(db);
  }

  async loginUser(payload) {
    const { username, password } = payload;

    const user = await this.query.findUser('SELECT * FROM users WHERE username=\''
    +username+ '\'');

    if (user.data.length < 1) {
      return wrapper.error(new NotFoundError('users not found'));
    }

    const userName = user.data[0].username;
    const pass = await commonUtil.decrypt(user.data[0].password, algorithm, secretKey);
    if (username !== userName || pass !== password) {
      return wrapper.error(new UnauthorizedError('password invalid!'));
    }

    const data = {
      username,
      sub: username
    };
    const token = await jwtAuth.generateToken(data);

    const response = {
      token,
      username
    };
    return wrapper.data(response);
  }

  async registerUser(payload) {
    const { username, password, name } = payload;

    const user = await this.query.findUser('SELECT * FROM users WHERE username=\''
    +username+ '\' AND name=\''+name+'\'');

    if (user.data.length > 0) {
      return wrapper.error(new BadRequestError('user already exist'));
    }

    const chiperPwd = await commonUtil.encrypt(password, algorithm, secretKey);

    let qy = 'INSERT INTO users (username,password,name) ' +
      'VALUES (\''
      + username + '\',\''
      + chiperPwd + '\',\''
      + name + '\')';

    const result = await this.command.insertUser(qy);

    if (result.err) {
      return wrapper.error(new BadRequestError(result.err));
    }

    return wrapper.data(result.data);
  }

  async updateUser(username, payload) {
    let { password, name } = payload;

    const user = await this.query.findUser('SELECT * FROM users WHERE username=\''
    +username+ '\'');

    if (user.data.length < 1) {
      return wrapper.error(new NotFoundError('user not found'));
    }

    !password? password = user.data[0].password : password = await commonUtil.encrypt(password, algorithm, secretKey);
    !name? name = user.data[0].name : name;

    let qy = 'UPDATE users SET ' +
    'password=\'' + password + '\',' +
    'name=\'' + name + '\'' +
    'WHERE username=\'' + username + '\'' ;

    const result = await this.command.updateUser(qy);
    if (result.err) {
      return wrapper.error(new BadRequestError(result.err));
    }

    return wrapper.data(result.data);
  }

  async deleteUser(username) {
    const user = await this.query.findUser('SELECT * FROM users WHERE username=\''
    +username+ '\'');

    if (user.data.length < 1) {
      return wrapper.error(new NotFoundError('user not found'));
    }

    let qy = 'DELETE FROM users WHERE username=\''+username+'\'';

    const result = await this.command.deleteUser(qy);
    if (result.err) {
      return wrapper.error(new BadRequestError(result.err));
    }

    return wrapper.data(result.data);
  }
}

module.exports = User;
