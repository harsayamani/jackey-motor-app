const User = require('./domain');
const Mysql = require('../../../../helpers/databases/mysql/db');
const config = require('../../../../infra/configs/global_config');

const db = new Mysql(config.get('/mysqlConfig'));
const user = new User(db);

const loginUser = async (payload) => {
  const postCommand = async (payload) => user.loginUser(payload);
  return postCommand(payload);
};

const registerUser = async (payload) => {
  const postCommand = async (payload) => user.registerUser(payload);
  return postCommand(payload);
};

const updateUser = async (username, payload) => {
  const postCommand = async (username, payload) => user.updateUser(username, payload);
  return postCommand(username, payload);
};

const deleteUser = async (username) => {
  const postCommand = async (username) => user.deleteUser(username);
  return postCommand(username);
};

module.exports = {
  loginUser,
  registerUser,
  updateUser,
  deleteUser
};
