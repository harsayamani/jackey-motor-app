const User = require('./domain');
const Mysql = require('../../../../helpers/databases/mysql/db');
const config = require('../../../../infra/configs/global_config');

const db = new Mysql(config.get('/mysqlConfig'));
const user = new User(db);

const getUser = async (username) => {
  const getData = async () => {
    const result = await user.viewUser(username);
    return result;
  };
  const result = await getData();
  return result;
};

const getAllUser= async () => {
  const getData = async () => {
    const result = await user.viewAllUser();
    return result;
  };
  const result = await getData();
  return result;
};

module.exports = {
  getUser,
  getAllUser,
};
