const Query = require('./query');
const wrapper = require('../../../../helpers/utils/wrapper');
const { NotFoundError, BadRequestError } = require('../../../../helpers/error');

class User {

  constructor(db) {
    this.query = new Query(db);
  }

  async viewUser(username) {
    try {
      const user = await this.query.findUser('SELECT * FROM users WHERE username=\''
        +username+ '\'');

      if (user.data.length < 1) {
        return wrapper.error(new NotFoundError('user not found'));
      }

      if (user.err) {
        return wrapper.error(new NotFoundError('there is an error query'));
      }

      return wrapper.data(user.data[0]);
    } catch (error) {
      return wrapper.error(new BadRequestError('invalid id'));
    }
  }

  async viewAllUser() {
    try {
      const user = await this.query.findUser('SELECT * FROM users');

      if (user.data.length < 1) {
        return wrapper.error(new NotFoundError('user not found'));
      }

      if (user.err) {
        return wrapper.error(new NotFoundError('there is an error query'));
      }

      return wrapper.data(user.data);
    } catch (error) {
      return wrapper.error(new BadRequestError(error));
    }
  }
}

module.exports = User;
