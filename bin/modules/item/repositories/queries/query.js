class Query {
  constructor(db) {
    this.db = db;
  }

  async findItem(statement) {
    const result = await this.db.query(statement);
    return result;
  }
}

module.exports = Query;

