const Query = require('./query');
const Service = require('../../../service/repositories/queries/query');
const Invoice = require('../../../invoice/repositories/queries/query');
const wrapper = require('../../../../helpers/utils/wrapper');
const { NotFoundError, BadRequestError } = require('../../../../helpers/error');

class Item {

  constructor(db) {
    this.query = new Query(db);
    this.service = new Service(db);
    this.invoice = new Invoice(db);
  }

  async viewItem(itemId) {
    try {
      const item = await this.query.findItem('SELECT * FROM item WHERE item_id=\''
        +itemId+ '\'');

      if (item.data.length < 1) {
        return wrapper.error(new NotFoundError('item not found'));
      }

      if (item.err) {
        return wrapper.error(new NotFoundError('there is an error query'));
      }

      const service = await this.service.findService('SELECT * FROM service WHERE service_id=\''
        +item.data[0].service_id+ '\'');

      if (service.data.length < 1) {
        return wrapper.error(new NotFoundError('service not found'));
      }

      if (service.err) {
        return wrapper.error(new NotFoundError('there is an error query'));
      }

      const data = {
        item_id: item.data[0].item_id,
        service: service.data[0],
        price: item.data[0].price,
        amount: item.data[0].amount,
        invoice_id: item.data[0].invoice_id,
      };

      return wrapper.data(data);
    } catch (error) {
      return wrapper.error(new BadRequestError(error));
    }
  }

  async viewAllItem() {
    try {
      const item = await this.query.findItem('SELECT * FROM item');

      if (item.data.length < 1) {
        return wrapper.error(new NotFoundError('item not found'));
      }

      if (item.err) {
        return wrapper.error(new NotFoundError('there is an error query'));
      }

      return wrapper.data(item.data);
    } catch (error) {
      return wrapper.error(new BadRequestError(error));
    }
  }
}

module.exports = Item;
