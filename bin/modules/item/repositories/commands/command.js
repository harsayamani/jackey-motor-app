class Command {
  constructor(db) {
    this.db = db;
  }

  async insertItem(statement) {
    const result = await this.db.query(statement);
    return result;
  }

  async updateItem(statement) {
    const result = await this.db.query(statement);
    return result;
  }

  async deleteItem(statement) {
    const result = await this.db.query(statement);
    return result;
  }
}

module.exports = Command;

