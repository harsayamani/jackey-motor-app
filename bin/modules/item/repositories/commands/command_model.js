const joi = require('joi');

const createItem = joi.object({
  service_id: joi.string().required(),
  qty: joi.number().required(),
  invoice_id: joi.string().required(),
});

const updateItem= joi.object({
  service_id: joi.string(),
  qty: joi.number(),
  invoice_id: joi.string(),
});

module.exports = {
  createItem,
  updateItem
};
