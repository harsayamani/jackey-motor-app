const Query = require('../queries/query');
const Service = require('../../../service/repositories/queries/query');
const Invoice = require('../../../invoice/repositories/queries/query');
const Command = require('./command');
const wrapper = require('../../../../helpers/utils/wrapper');
const randomstring = require('randomstring');
const { NotFoundError, BadRequestError } = require('../../../../helpers/error');

class Item {
  constructor(db) {
    this.command = new Command(db);
    this.query = new Query(db);
    this.service = new Service(db);
    this.invoice = new Invoice(db);
  }

  async insertItem(payload) {
    const { service_id, qty, invoice_id } = payload;

    const service = await this.service.findService('SELECT * FROM service WHERE service_id=\''
    +service_id+ '\'');

    if (service.data.length < 1) {
      return wrapper.error(new NotFoundError('service not found'));
    }

    const invoice = await this.invoice.findInvoice('SELECT * FROM invoice WHERE invoice_id=\''
    +invoice_id+ '\'');

    if (invoice.data.length < 1) {
      return wrapper.error(new NotFoundError('invoice not found'));
    }

    const itemId = 'ITEM-'+randomstring.generate(6);
    let price = service.data[0].price;
    let amount = qty * price;

    let qy = 'INSERT INTO item (item_id,service_id,qty,price,amount,invoice_id) ' +
      'VALUES (\''
      + itemId + '\',\''
      + service_id + '\',\''
      + qty + '\',\''
      + price + '\',\''
      + amount + '\',\''
      + invoice_id + '\')';

    const result = await this.command.insertItem(qy);

    if (result.err) {
      return wrapper.error(new BadRequestError(result.err));
    }

    return wrapper.data(result.data);
  }

  async updateItem(itemId, payload) {
    let { service_id, qty, invoice_id } = payload;

    const item = await this.query.findItem('SELECT * FROM item WHERE item_id=\''
    +itemId+ '\'');

    if (item.data.length < 1) {
      return wrapper.error(new NotFoundError('item not found'));
    }

    let price;
    let amount;

    if(!service_id) {
      service_id = item.data[0].service_id;
    }else {
      service_id;
    }

    if(!qty) {
      qty = item.data[0].qty;
    }else {
      qty;
    }

    !invoice_id? invoice_id = item.data[0].invoice_id : invoice_id;

    const service = await this.service.findService('SELECT * FROM service WHERE service_id=\''
    +service_id+ '\'');

    if (service.data.length < 1) {
      return wrapper.error(new NotFoundError('service not found'));
    }

    const invoice = await this.invoice.findInvoice('SELECT * FROM invoice WHERE invoice_id=\''
    +invoice_id+ '\'');

    if (invoice.data.length < 1) {
      return wrapper.error(new NotFoundError('invoice not found'));
    }

    price = service.data[0].price;
    amount = price * qty;

    let qy = 'UPDATE item SET ' +
    'service_id=\'' + service_id + '\',' +
    'qty=\'' + qty + '\',' +
    'price=\'' + price + '\',' +
    'amount=\'' + amount + '\'' +
    'WHERE item_id=\'' + itemId + '\'' ;

    const result = await this.command.updateItem(qy);
    if (result.err) {
      return wrapper.error(new BadRequestError(result.err));
    }

    return wrapper.data(result.data);
  }

  async deleteItem(itemId) {
    const item = await this.query.findItem('SELECT * FROM item WHERE item_id=\''
    +itemId+ '\'');

    if (item.data.length < 1) {
      return wrapper.error(new NotFoundError('item not found'));
    }

    let qy = 'DELETE FROM item WHERE item_id=\''+itemId+'\'';

    const result = await this.command.deleteItem(qy);
    if (result.err) {
      return wrapper.error(new BadRequestError(result.err));
    }

    return wrapper.data(result.data);
  }
}

module.exports = Item;
