class Query {
  constructor(db) {
    this.db = db;
  }

  async findService(statement) {
    const result = await this.db.query(statement);
    return result;
  }
}

module.exports = Query;

