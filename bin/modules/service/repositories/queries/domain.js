const Query = require('./query');
const wrapper = require('../../../../helpers/utils/wrapper');
const { NotFoundError, BadRequestError } = require('../../../../helpers/error');

class Service {

  constructor(db) {
    this.query = new Query(db);
  }

  async viewService(serviceId) {
    try {
      const service = await this.query.findService('SELECT * FROM service WHERE service_id=\''
        +serviceId+ '\'');

      if (service.data.length < 1) {
        return wrapper.error(new NotFoundError('service not found'));
      }

      if (service.err) {
        return wrapper.error(new NotFoundError('there is an error query'));
      }

      return wrapper.data(service.data[0]);
    } catch (error) {
      return wrapper.error(new BadRequestError('invalid id'));
    }
  }

  async viewAllService() {
    try {
      const service = await this.query.findService('SELECT * FROM service');

      if (service.data.length < 1) {
        return wrapper.error(new NotFoundError('service not found'));
      }

      if (service.err) {
        return wrapper.error(new NotFoundError('there is an error query'));
      }

      return wrapper.data(service.data);
    } catch (error) {
      return wrapper.error(new BadRequestError(error));
    }
  }
}

module.exports = Service;

