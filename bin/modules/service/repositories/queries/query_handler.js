const Service = require('./domain');
const Mysql = require('../../../../helpers/databases/mysql/db');
const config = require('../../../../infra/configs/global_config');

const db = new Mysql(config.get('/mysqlConfig'));
const service = new Service(db);

const getService = async (serviceId) => {
  const getData = async () => {
    const result = await service.viewService(serviceId);
    return result;
  };
  const result = await getData();
  return result;
};

const getAllService = async () => {
  const getData = async () => {
    const result = await service.viewAllService();
    return result;
  };
  const result = await getData();
  return result;
};

module.exports = {
  getService,
  getAllService,
};
