const Query = require('../queries/query');
const Command = require('./command');
const wrapper = require('../../../../helpers/utils/wrapper');
const randomstring = require('randomstring');
const { NotFoundError, BadRequestError } = require('../../../../helpers/error');

class Service {
  constructor(db) {
    this.command = new Command(db);
    this.query = new Query(db);
  }

  async insertService(payload) {
    const { serv_supl, price } = payload;

    const service = await this.query.findService('SELECT * FROM service WHERE serv_supl=\''
    +serv_supl+ '\' AND price=\''+price+'\'');

    if (service.data.length > 0) {
      return wrapper.error(new BadRequestError('service already exist'));
    }

    const serviceId = 'SS-'+randomstring.generate(6);

    let qy = 'INSERT INTO service (service_id,serv_supl,price) ' +
      'VALUES (\''
      + serviceId + '\',\''
      + serv_supl + '\',\''
      + price + '\')';

    const result = await this.command.insertService(qy);

    if (result.err) {
      return wrapper.error(new BadRequestError(result.err));
    }

    return wrapper.data(result.data);
  }

  async updateService(serviceId, payload) {
    let { serv_supl, price } = payload;

    const service = await this.query.findService('SELECT * FROM service WHERE service_id=\''
    +serviceId+ '\'');

    if (service.data.length < 1) {
      return wrapper.error(new NotFoundError('service not found'));
    }

    !serv_supl? serv_supl = service.data[0].serv_supl : serv_supl;
    !price? price = service.data[0].price : price;

    let qy = 'UPDATE service SET ' +
    'serv_supl=\'' + serv_supl + '\',' +
    'price=\'' + price + '\'' +
    'WHERE service_id=\'' + serviceId + '\'' ;

    const result = await this.command.updateService(qy);
    if (result.err) {
      return wrapper.error(new BadRequestError(result.err));
    }

    return wrapper.data(result.data);
  }

  async deleteService(serviceId) {
    const service = await this.query.findService('SELECT * FROM service WHERE service_id=\''
    +serviceId+ '\'');

    if (service.data.length < 1) {
      return wrapper.error(new NotFoundError('service not found'));
    }

    let qy = 'DELETE FROM service WHERE service_id=\''+serviceId+'\'';

    const result = await this.command.deleteService(qy);
    if (result.err) {
      return wrapper.error(new BadRequestError(result.err));
    }

    return wrapper.data(result.data);
  }
}

module.exports = Service;
