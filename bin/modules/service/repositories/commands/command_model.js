const joi = require('joi');

const createService = joi.object({
  serv_supl: joi.string().required(),
  price: joi.number().required(),
});

const updateService = joi.object({
  serv_supl: joi.string(),
  price: joi.number(),
});

module.exports = {
  createService,
  updateService
};
