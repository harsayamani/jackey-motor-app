class Command {
  constructor(db) {
    this.db = db;
  }

  async insertService(statement) {
    const result = await this.db.query(statement);
    return result;
  }

  async updateService(statement) {
    const result = await this.db.query(statement);
    return result;
  }

  async deleteService(statement) {
    const result = await this.db.query(statement);
    return result;
  }
}

module.exports = Command;

