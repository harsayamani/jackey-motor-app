const Service = require('./domain');
const Mysql = require('../../../../helpers/databases/mysql/db');
const config = require('../../../../infra/configs/global_config');

const db = new Mysql(config.get('/mysqlConfig'));
const service = new Service(db);

const createService = async (payload) => {
  const postCommand = async (payload) => service.insertService(payload);
  return postCommand(payload);
};

const updateService = async (serviceId, payload) => {
  const postCommand = async (serviceId, payload) => service.updateService(serviceId, payload);
  return postCommand(serviceId, payload);
};

const deleteService = async (serviceId) => {
  const postCommand = async (serviceId) => service.deleteService(serviceId);
  return postCommand(serviceId);
};

module.exports = {
  createService,
  updateService,
  deleteService
};
