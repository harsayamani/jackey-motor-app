const wrapper = require('../../../helpers/utils/wrapper');
const commandHandler = require('../repositories/commands/command_handler');
const commandModel = require('../repositories/commands/command_model');
const queryHandler = require('../repositories/queries/query_handler');
const validator = require('../utils/validator');
const {
  ERROR: httpError,
  SUCCESS: http,
} = require('../../../helpers/http-status/status_code');

const createService = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(
    payload,
    commandModel.createService,
  );
  const postRequest = async (result) => {
    if (result.err) {
      return result;
    }
    return commandHandler.createService(result.data);
  };
  const sendResponse = async (result) => {
    /* eslint no-unused-expressions: [2, { allowTernary: true }] */
    result.err
      ? wrapper.response(
        res,
        'fail',
        result,
        'Create Service Fail',
        httpError.BAD_REQUEST,
      )
      : wrapper.response(res, 'success', result, 'Create Service Success', http.OK);
  };
  sendResponse(await postRequest(validatePayload));
};

const updateService = async (req, res) => {
  const { serviceId } = req.params;

  const payload = req.body;
  const validatePayload = validator.isValidPayload(
    payload,
    commandModel.updateService,
  );
  const postRequest = async (result) => {
    if (result.err) {
      return result;
    }
    return commandHandler.updateService(serviceId, result.data);
  };
  const sendResponse = async (result) => {
    /* eslint no-unused-expressions: [2, { allowTernary: true }] */
    result.err
      ? wrapper.response(
        res,
        'fail',
        result,
        'Update Service Fail',
        httpError.BAD_REQUEST,
      )
      : wrapper.response(res, 'success', result, 'Update Service Success', http.OK);
  };
  sendResponse(await postRequest(validatePayload));
};

const deleteService = async (req, res) => {
  const { serviceId } = req.params;

  const postRequest = async () => commandHandler.deleteService(serviceId);
  const sendResponse = async (result) => {
    /* eslint no-unused-expressions: [2, { allowTernary: true }] */
    result.err
      ? wrapper.response(
        res,
        'fail',
        result,
        'Delete Service Fail',
        httpError.NOT_FOUND,
      )
      : wrapper.response(res, 'success', result, 'Delete Service Success', http.OK);
  };
  sendResponse(await postRequest());
};

const getService = async (req, res) => {
  const { serviceId } = req.params;

  const getData = async () => queryHandler.getService(serviceId);
  const sendResponse = async (result) => {
    result.err
      ? wrapper.response(res, 'fail', result, 'Get Service Fail', httpError.NOT_FOUND)
      : wrapper.response(res, 'success', result, 'Get Service Success', http.OK);
  };
  sendResponse(await getData());
};

const getAllService = async (req, res) => {
//   const { testId } = req.query;
  const getData = async () => queryHandler.getAllService();
  const sendResponse = async (result) => {
    result.err
      ? wrapper.response(
        res,
        'fail',
        result,
        'Get All Service Fail',
        httpError.NOT_FOUND,
      )
      : wrapper.response(res, 'success', result, 'Get All Service Success', http.OK);
  };
  sendResponse(await getData());
};

module.exports = {
  createService,
  updateService,
  deleteService,
  getService,
  getAllService
};
