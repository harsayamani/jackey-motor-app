const wrapper = require('../../../helpers/utils/wrapper');
const commandHandler = require('../repositories/commands/command_handler');
const commandModel = require('../repositories/commands/command_model');
const queryHandler = require('../repositories/queries/query_handler');
const validator = require('../utils/validator');
const {
  ERROR: httpError,
  SUCCESS: http,
} = require('../../../helpers/http-status/status_code');

const createCustomer = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(
    payload,
    commandModel.createCustomer,
  );
  const postRequest = async (result) => {
    if (result.err) {
      return result;
    }
    return commandHandler.createCustomer(result.data);
  };
  const sendResponse = async (result) => {
    /* eslint no-unused-expressions: [2, { allowTernary: true }] */
    result.err
      ? wrapper.response(
        res,
        'fail',
        result,
        'Create Customer Fail',
        httpError.BAD_REQUEST,
      )
      : wrapper.response(res, 'success', result, 'Create Customer Success', http.OK);
  };
  sendResponse(await postRequest(validatePayload));
};

const updateCustomer = async (req, res) => {
  const { custId } = req.params;

  const payload = req.body;
  const validatePayload = validator.isValidPayload(
    payload,
    commandModel.updateCustomer,
  );
  const postRequest = async (result) => {
    if (result.err) {
      return result;
    }
    return commandHandler.updateCustomer(custId, result.data);
  };
  const sendResponse = async (result) => {
    /* eslint no-unused-expressions: [2, { allowTernary: true }] */
    result.err
      ? wrapper.response(
        res,
        'fail',
        result,
        'Update Customer Fail',
        httpError.BAD_REQUEST,
      )
      : wrapper.response(res, 'success', result, 'Update Customer Success', http.OK);
  };
  sendResponse(await postRequest(validatePayload));
};

const deleteCustomer = async (req, res) => {
  const { custId } = req.params;

  const postRequest = async () => commandHandler.deleteCustomer(custId);
  const sendResponse = async (result) => {
    /* eslint no-unused-expressions: [2, { allowTernary: true }] */
    result.err
      ? wrapper.response(
        res,
        'fail',
        result,
        'Delete Customer Fail',
        httpError.NOT_FOUND,
      )
      : wrapper.response(res, 'success', result, 'Delete Customer Success', http.OK);
  };
  sendResponse(await postRequest());
};

const getCustomer = async (req, res) => {
  const { custId } = req.params;

  const getData = async () => queryHandler.getCustomer(custId);
  const sendResponse = async (result) => {
    result.err
      ? wrapper.response(res, 'fail', result, 'Get Customer Fail', httpError.NOT_FOUND)
      : wrapper.response(res, 'success', result, 'Get Customer Success', http.OK);
  };
  sendResponse(await getData());
};

const getAllCustomer = async (req, res) => {
//   const { testId } = req.query;
  const getData = async () => queryHandler.getAllCustomer();
  const sendResponse = async (result) => {
    result.err
      ? wrapper.response(
        res,
        'fail',
        result,
        'Get All Customer Fail',
        httpError.NOT_FOUND,
      )
      : wrapper.response(res, 'success', result, 'Get All Customer Success', http.OK);
  };
  sendResponse(await getData());
};

module.exports = {
  createCustomer,
  updateCustomer,
  deleteCustomer,
  getCustomer,
  getAllCustomer
};
