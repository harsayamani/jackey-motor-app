const Customer = require('./domain');
const Mysql = require('../../../../helpers/databases/mysql/db');
const config = require('../../../../infra/configs/global_config');

const db = new Mysql(config.get('/mysqlConfig'));
const cust = new Customer(db);

const createCustomer = async (payload) => {
  const postCommand = async (payload) => cust.insertCustomer(payload);
  return postCommand(payload);
};

const updateCustomer = async (custId, payload) => {
  const postCommand = async (custId, payload) => cust.updateCustomer(custId, payload);
  return postCommand(custId, payload);
};

const deleteCustomer = async (custId) => {
  const postCommand = async (custId) => cust.deleteCustomer(custId);
  return postCommand(custId);
};

module.exports = {
  createCustomer,
  updateCustomer,
  deleteCustomer
};
