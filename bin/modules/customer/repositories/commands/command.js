class Command {
  constructor(db) {
    this.db = db;
  }

  async insertCustomer(statement) {
    const result = await this.db.query(statement);
    return result;
  }

  async updateCustomer(statement) {
    const result = await this.db.query(statement);
    return result;
  }

  async deleteCustomer(statement) {
    const result = await this.db.query(statement);
    return result;
  }
}

module.exports = Command;

