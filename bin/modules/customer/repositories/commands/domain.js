const Query = require('../queries/query');
const Command = require('./command');
const wrapper = require('../../../../helpers/utils/wrapper');
const randomstring = require('randomstring');
const { NotFoundError, BadRequestError } = require('../../../../helpers/error');

class Customer {
  constructor(db) {
    this.command = new Command(db);
    this.query = new Query(db);
  }

  async insertCustomer(payload) {
    const { name, phone_number, address } = payload;

    const customer = await this.query.findCustomer('SELECT * FROM customer WHERE name=\''
    +name+ '\'');

    if (customer.data.length > 0) {
      return wrapper.error(new BadRequestError('customer already exist'));
    }

    const custId = 'CUST-'+randomstring.generate(6);

    let qy = 'INSERT INTO customer (cust_id,name,phone_number,address) ' +
      'VALUES (\''
      + custId + '\',\''
      + name + '\',\''
      + phone_number + '\',\''
      + address + '\')';

    const result = await this.command.insertCustomer(qy);

    if (result.err) {
      return wrapper.error(new BadRequestError(result.err));
    }

    return wrapper.data(result.data);
  }

  async updateCustomer(custId, payload) {
    let { name, phone_number, address } = payload;

    const customer = await this.query.findCustomer('SELECT * FROM customer WHERE cust_id=\''
    +custId+ '\'');

    if (customer.data.length < 1) {
      return wrapper.error(new NotFoundError('customer not found'));
    }

    !name? name = customer.data[0].name : name;
    !phone_number? phone_number = customer.data[0].phone_number : phone_number;
    !address? address = customer.data[0].address : address;

    let qy = 'UPDATE customer SET ' +
    'name=\'' + name + '\',' +
    'phone_number=\'' + phone_number + '\',' +
    'address=\'' + address + '\'' +
    'WHERE cust_id=\'' + custId + '\'' ;

    const result = await this.command.updateCustomer(qy);
    if (result.err) {
      return wrapper.error(new BadRequestError(result.err));
    }

    return wrapper.data(result.data);
  }

  async deleteCustomer(custId) {
    const customer = await this.query.findCustomer('SELECT * FROM customer WHERE cust_id=\''
    +custId+ '\'');

    if (customer.data.length < 1) {
      return wrapper.error(new NotFoundError('customer not found'));
    }

    let qy = 'DELETE FROM customer WHERE cust_id=\''+custId+'\'';

    const result = await this.command.deleteCustomer(qy);
    if (result.err) {
      return wrapper.error(new BadRequestError(result.err));
    }

    return wrapper.data(result.data);
  }
}

module.exports = Customer;
