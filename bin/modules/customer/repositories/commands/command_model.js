const joi = require('joi');

const createCustomer = joi.object({
  name: joi.string().required(),
  phone_number: joi.string().required(),
  address: joi.string().required(),
});

const updateCustomer = joi.object({
  name: joi.string(),
  phone_number: joi.string(),
  address: joi.string()
});

module.exports = {
  createCustomer,
  updateCustomer
};
