const Query = require('./query');
const wrapper = require('../../../../helpers/utils/wrapper');
const { NotFoundError, BadRequestError } = require('../../../../helpers/error');

class Customer {

  constructor(db) {
    this.query = new Query(db);
  }

  async viewCustomer(custId) {
    try {
      const customer = await this.query.findCustomer('SELECT * FROM customer WHERE cust_id=\''
        +custId+ '\'');

      if (customer.data.length < 1) {
        return wrapper.error(new NotFoundError('customer not found'));
      }

      if (customer.err) {
        return wrapper.error(new NotFoundError('there is an error query'));
      }

      return wrapper.data(customer.data[0]);
    } catch (error) {
      return wrapper.error(new BadRequestError('invalid id'));
    }
  }

  async viewAllCustomer() {
    try {
      const customer = await this.query.findCustomer('SELECT * FROM customer');

      if (customer.data.length < 1) {
        return wrapper.error(new NotFoundError('customer not found'));
      }

      if (customer.err) {
        return wrapper.error(new NotFoundError('there is an error query'));
      }

      return wrapper.data(customer.data);
    } catch (error) {
      return wrapper.error(new BadRequestError(error));
    }
  }
}

module.exports = Customer;
