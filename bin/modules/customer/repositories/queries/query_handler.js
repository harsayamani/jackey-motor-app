const Customer = require('./domain');
const Mysql = require('../../../../helpers/databases/mysql/db');
const config = require('../../../../infra/configs/global_config');

const db = new Mysql(config.get('/mysqlConfig'));
const cust = new Customer(db);

const getCustomer = async (custId) => {
  const getData = async () => {
    const result = await cust.viewCustomer(custId);
    return result;
  };
  const result = await getData();
  return result;
};

const getAllCustomer = async () => {
  const getData = async () => {
    const result = await cust.viewAllCustomer();
    return result;
  };
  const result = await getData();
  return result;
};

module.exports = {
  getCustomer,
  getAllCustomer,
};
