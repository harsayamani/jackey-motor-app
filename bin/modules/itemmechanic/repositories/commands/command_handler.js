const Item = require('./domain');
const Mysql = require('../../../../helpers/databases/mysql/db');
const config = require('../../../../infra/configs/global_config');

const db = new Mysql(config.get('/mysqlConfig'));
const item = new Item(db);

const createItem = async (payload) => {
  const postCommand = async (payload) => item.insertItem(payload);
  return postCommand(payload);
};

const updateItem = async (itemId, payload) => {
  const postCommand = async (itemId, payload) => item.updateItem(itemId, payload);
  return postCommand(itemId, payload);
};

const deleteItem = async (itemId) => {
  const postCommand = async (itemId) => item.deleteItem(itemId);
  return postCommand(itemId);
};

module.exports = {
  createItem,
  updateItem,
  deleteItem
};
