const Item = require('./domain');
const Mysql = require('../../../../helpers/databases/mysql/db');
const config = require('../../../../infra/configs/global_config');

const db = new Mysql(config.get('/mysqlConfig'));
const item = new Item(db);

const getItem = async (itemId) => {
  const getData = async () => {
    const result = await item.viewItem(itemId);
    return result;
  };
  const result = await getData();
  return result;
};

const getAllItem = async () => {
  const getData = async () => {
    const result = await item.viewAllItem();
    return result;
  };
  const result = await getData();
  return result;
};

module.exports = {
  getItem,
  getAllItem,
};
