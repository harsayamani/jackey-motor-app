const wrapper = require('../../../helpers/utils/wrapper');
const commandHandler = require('../repositories/commands/command_handler');
const commandModel = require('../repositories/commands/command_model');
const queryHandler = require('../repositories/queries/query_handler');
const validator = require('../utils/validator');
const {
  ERROR: httpError,
  SUCCESS: http,
} = require('../../../helpers/http-status/status_code');

const createItem = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(
    payload,
    commandModel.createItem,
  );
  const postRequest = async (result) => {
    if (result.err) {
      return result;
    }
    return commandHandler.createItem(result.data);
  };
  const sendResponse = async (result) => {
    /* eslint no-unused-expressions: [2, { allowTernary: true }] */
    result.err
      ? wrapper.response(
        res,
        'fail',
        result,
        'Create Item Fail',
        httpError.BAD_REQUEST,
      )
      : wrapper.response(res, 'success', result, 'Create Item Success', http.OK);
  };
  sendResponse(await postRequest(validatePayload));
};

const updateItem = async (req, res) => {
  const { itemId } = req.params;

  const payload = req.body;
  const validatePayload = validator.isValidPayload(
    payload,
    commandModel.updateItem,
  );
  const postRequest = async (result) => {
    if (result.err) {
      return result;
    }
    return commandHandler.updateItem(itemId, result.data);
  };
  const sendResponse = async (result) => {
    /* eslint no-unused-expressions: [2, { allowTernary: true }] */
    result.err
      ? wrapper.response(
        res,
        'fail',
        result,
        'Update Item Fail',
        httpError.BAD_REQUEST,
      )
      : wrapper.response(res, 'success', result, 'Update Item Success', http.OK);
  };
  sendResponse(await postRequest(validatePayload));
};

const deleteItem = async (req, res) => {
  const { itemId } = req.params;

  const postRequest = async () => commandHandler.deleteItem(itemId);
  const sendResponse = async (result) => {
    /* eslint no-unused-expressions: [2, { allowTernary: true }] */
    result.err
      ? wrapper.response(
        res,
        'fail',
        result,
        'Delete Item Fail',
        httpError.NOT_FOUND,
      )
      : wrapper.response(res, 'success', result, 'Delete Item Success', http.OK);
  };
  sendResponse(await postRequest());
};

const getItem = async (req, res) => {
  const { itemId } = req.params;

  const getData = async () => queryHandler.getItem(itemId);
  const sendResponse = async (result) => {
    result.err
      ? wrapper.response(res, 'fail', result, 'Get Item Fail', httpError.NOT_FOUND)
      : wrapper.response(res, 'success', result, 'Get Item Success', http.OK);
  };
  sendResponse(await getData());
};

const getAllItem = async (req, res) => {
//   const { testId } = req.query;
  const getData = async () => queryHandler.getAllItem();
  const sendResponse = async (result) => {
    result.err
      ? wrapper.response(
        res,
        'fail',
        result,
        'Get All Item Fail',
        httpError.NOT_FOUND,
      )
      : wrapper.response(res, 'success', result, 'Get All Item Success', http.OK);
  };
  sendResponse(await getData());
};

module.exports = {
  createItem,
  updateItem,
  deleteItem,
  getItem,
  getAllItem
};
