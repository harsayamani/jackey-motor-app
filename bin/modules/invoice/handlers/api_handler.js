const wrapper = require('../../../helpers/utils/wrapper');
const commandHandler = require('../repositories/commands/command_handler');
const commandModel = require('../repositories/commands/command_model');
const queryHandler = require('../repositories/queries/query_handler');
const validator = require('../utils/validator');
const {
  ERROR: httpError,
  SUCCESS: http,
} = require('../../../helpers/http-status/status_code');

const createInvoice = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(
    payload,
    commandModel.createInvoice,
  );
  const postRequest = async (result) => {
    if (result.err) {
      return result;
    }
    return commandHandler.createInvoice(result.data);
  };
  const sendResponse = async (result) => {
    /* eslint no-unused-expressions: [2, { allowTernary: true }] */
    result.err
      ? wrapper.response(
        res,
        'fail',
        result,
        'Create Invoice Fail',
        httpError.BAD_REQUEST,
      )
      : wrapper.response(res, 'success', result, 'Create Invoice Success', http.OK);
  };
  sendResponse(await postRequest(validatePayload));
};

const updateInvoice = async (req, res) => {
  const { invoiceId } = req.params;

  const payload = req.body;
  const validatePayload = validator.isValidPayload(
    payload,
    commandModel.updateInvoice,
  );
  const postRequest = async (result) => {
    if (result.err) {
      return result;
    }
    return commandHandler.updateInvoice(invoiceId, result.data);
  };
  const sendResponse = async (result) => {
    /* eslint no-unused-expressions: [2, { allowTernary: true }] */
    result.err
      ? wrapper.response(
        res,
        'fail',
        result,
        'Update Invoice Fail',
        httpError.BAD_REQUEST,
      )
      : wrapper.response(res, 'success', result, 'Update Invoice Success', http.OK);
  };
  sendResponse(await postRequest(validatePayload));
};

const deleteInvoice = async (req, res) => {
  const { invoiceId } = req.params;

  const postRequest = async () => commandHandler.deleteInvoice(invoiceId);
  const sendResponse = async (result) => {
    /* eslint no-unused-expressions: [2, { allowTernary: true }] */
    result.err
      ? wrapper.response(
        res,
        'fail',
        result,
        'Delete Invoice Fail',
        httpError.NOT_FOUND,
      )
      : wrapper.response(res, 'success', result, 'Delete Invoice Success', http.OK);
  };
  sendResponse(await postRequest());
};

const getInvoice = async (req, res) => {
  const { invoiceId } = req.params;

  const getData = async () => queryHandler.getInvoice(invoiceId);
  const sendResponse = async (result) => {
    result.err
      ? wrapper.response(res, 'fail', result, 'Get Invoice Fail', httpError.NOT_FOUND)
      : wrapper.response(res, 'success', result, 'Get Invoice Success', http.OK);
  };
  sendResponse(await getData());
};

const getAllInvoice = async (req, res) => {
//   const { testId } = req.query;
  const getData = async () => queryHandler.getAllInvoice();
  const sendResponse = async (result) => {
    result.err
      ? wrapper.response(
        res,
        'fail',
        result,
        'Get All Invoice Fail',
        httpError.NOT_FOUND,
      )
      : wrapper.response(res, 'success', result, 'Get All Invoice Success', http.OK);
  };
  sendResponse(await getData());
};

module.exports = {
  createInvoice,
  updateInvoice,
  deleteInvoice,
  getInvoice,
  getAllInvoice
};
