class Command {
  constructor(db) {
    this.db = db;
  }

  async insertInvoice(statement) {
    const result = await this.db.query(statement);
    return result;
  }

  async updateInvoice(statement) {
    const result = await this.db.query(statement);
    return result;
  }

  async deleteInvoice(statement) {
    const result = await this.db.query(statement);
    return result;
  }
}

module.exports = Command;

