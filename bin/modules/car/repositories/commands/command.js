class Command {
  constructor(db) {
    this.db = db;
  }

  async insertCar(statement) {
    const result = await this.db.query(statement);
    return result;
  }

  async updateCar(statement) {
    const result = await this.db.query(statement);
    return result;
  }

  async deleteCar(statement) {
    const result = await this.db.query(statement);
    return result;
  }
}

module.exports = Command;

