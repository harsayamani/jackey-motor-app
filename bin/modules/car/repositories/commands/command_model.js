const joi = require('joi');

const createCar = joi.object({
  model: joi.string().required(),
  variant: joi.string().required(),
  merk: joi.string().required(),
  prod_year: joi.string().required()
});

const updateCar = joi.object({
  model: joi.string(),
  variant: joi.string(),
  merk: joi.string(),
  prod_year: joi.string()
});

module.exports = {
  createCar,
  updateCar
};
