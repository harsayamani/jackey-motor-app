const Query = require('../queries/query');
const Command = require('./command');
const wrapper = require('../../../../helpers/utils/wrapper');
const randomstring = require('randomstring');
const { NotFoundError, BadRequestError } = require('../../../../helpers/error');

class Car {
  constructor(db) {
    this.command = new Command(db);
    this.query = new Query(db);
  }

  async insertCar(payload) {
    const { model, variant, merk, prod_year } = payload;

    const car = await this.query.findCar('SELECT * FROM car WHERE model=\''
    +model+ '\' AND variant=\''+variant+'\'');

    if (car.data.length > 0) {
      return wrapper.error(new BadRequestError('car already exist'));
    }

    const carId = 'CAR-'+randomstring.generate(6);

    let qy = 'INSERT INTO car (car_id,model,variant,merk,prod_year) ' +
      'VALUES (\''
      + carId + '\',\''
      + model + '\',\''
      + variant + '\',\''
      + merk + '\',\''
      + prod_year + '\')';

    const result = await this.command.insertCar(qy);

    if (result.err) {
      return wrapper.error(new BadRequestError(result.err));
    }

    return wrapper.data(result.data);
  }

  async updateCar(carId, payload) {
    let { model, variant, merk, prod_year } = payload;

    const car = await this.query.findCar('SELECT * FROM car WHERE car_id=\''
    +carId+ '\'');

    if (car.data.length < 1) {
      return wrapper.error(new NotFoundError('car not found'));
    }

    !model? model = car.data[0].model : model;
    !variant? variant = car.data[0].variant : variant;
    !merk? merk = car.data[0].merk :merk;
    !prod_year? prod_year = car.data[0].prod_year :prod_year;

    let qy = 'UPDATE car SET ' +
    'model=\'' + model + '\',' +
    'variant=\'' + variant + '\',' +
    'merk=\'' + merk + '\',' +
    'prod_year=\'' + prod_year + '\'' +
    'WHERE car_id=\'' + carId + '\'' ;

    const result = await this.command.updateCar(qy);
    if (result.err) {
      return wrapper.error(new BadRequestError(result.err));
    }

    return wrapper.data(result.data);
  }

  async deleteCar(carId) {
    const car = await this.query.findCar('SELECT * FROM car WHERE car_id=\''
    +carId+ '\'');

    if (car.data.length < 1) {
      return wrapper.error(new NotFoundError('car not found'));
    }

    let qy = 'DELETE FROM car WHERE car_id=\''+carId+'\'';

    const result = await this.command.deleteCar(qy);
    if (result.err) {
      return wrapper.error(new BadRequestError(result.err));
    }

    return wrapper.data(result.data);
  }
}

module.exports = Car;
