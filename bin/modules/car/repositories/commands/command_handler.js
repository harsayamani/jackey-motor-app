const Car = require('./domain');
const Mysql = require('../../../../helpers/databases/mysql/db');
const config = require('../../../../infra/configs/global_config');

const db = new Mysql(config.get('/mysqlConfig'));
const car = new Car(db);

const createCar = async (payload) => {
  const postCommand = async (payload) => car.insertCar(payload);
  return postCommand(payload);
};

const updateCar = async (carId, payload) => {
  const postCommand = async (carId, payload) => car.updateCar(carId, payload);
  return postCommand(carId, payload);
};

const deleteCar = async (carId) => {
  const postCommand = async (carId) => car.deleteCar(carId);
  return postCommand(carId);
};

module.exports = {
  createCar,
  updateCar,
  deleteCar
};
