const Car = require('./domain');
const Mysql = require('../../../../helpers/databases/mysql/db');
const config = require('../../../../infra/configs/global_config');

const db = new Mysql(config.get('/mysqlConfig'));
const car = new Car(db);

const getCar = async (carId) => {
  const getData = async () => {
    const result = await car.viewCar(carId);
    return result;
  };
  const result = await getData();
  return result;
};

const getAllCar = async () => {
  const getData = async () => {
    const result = await car.viewAllCar();
    return result;
  };
  const result = await getData();
  return result;
};

module.exports = {
  getCar,
  getAllCar,
};
