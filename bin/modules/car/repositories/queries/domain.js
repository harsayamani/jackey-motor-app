const Query = require('./query');
const wrapper = require('../../../../helpers/utils/wrapper');
const { NotFoundError, BadRequestError } = require('../../../../helpers/error');

class Car {

  constructor(db) {
    this.query = new Query(db);
  }

  async viewCar(carId) {
    try {
      const car = await this.query.findCar('SELECT * FROM car WHERE car_id=\''
        +carId+ '\'');

      if (car.data.length < 1) {
        return wrapper.error(new NotFoundError('car not found'));
      }

      if (car.err) {
        return wrapper.error(new NotFoundError('there is an error query'));
      }

      return wrapper.data(car.data[0]);
    } catch (error) {
      return wrapper.error(new BadRequestError('invalid id'));
    }
  }

  async viewAllCar() {
    try {
      const car = await this.query.findCar('SELECT * FROM car');

      if (car.data.length < 1) {
        return wrapper.error(new NotFoundError('car not found'));
      }

      if (car.err) {
        return wrapper.error(new NotFoundError('there is an error query'));
      }

      return wrapper.data(car.data);
    } catch (error) {
      return wrapper.error(new BadRequestError(error));
    }
  }
}

module.exports = Car;
