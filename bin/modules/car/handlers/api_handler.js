const wrapper = require('../../../helpers/utils/wrapper');
const commandHandler = require('../repositories/commands/command_handler');
const commandModel = require('../repositories/commands/command_model');
const queryHandler = require('../repositories/queries/query_handler');
const validator = require('../utils/validator');
const {
  ERROR: httpError,
  SUCCESS: http,
} = require('../../../helpers/http-status/status_code');

const createCar = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(
    payload,
    commandModel.createCar,
  );
  const postRequest = async (result) => {
    if (result.err) {
      return result;
    }
    return commandHandler.createCar(result.data);
  };
  const sendResponse = async (result) => {
    /* eslint no-unused-expressions: [2, { allowTernary: true }] */
    result.err
      ? wrapper.response(
        res,
        'fail',
        result,
        'Create Car Fail',
        httpError.BAD_REQUEST,
      )
      : wrapper.response(res, 'success', result, 'Create Car Success', http.OK);
  };
  sendResponse(await postRequest(validatePayload));
};

const updateCar = async (req, res) => {
  const { carId } = req.params;

  const payload = req.body;
  const validatePayload = validator.isValidPayload(
    payload,
    commandModel.updateCar,
  );
  const postRequest = async (result) => {
    if (result.err) {
      return result;
    }
    return commandHandler.updateCar(carId, result.data);
  };
  const sendResponse = async (result) => {
    /* eslint no-unused-expressions: [2, { allowTernary: true }] */
    result.err
      ? wrapper.response(
        res,
        'fail',
        result,
        'Update Car Fail',
        httpError.BAD_REQUEST,
      )
      : wrapper.response(res, 'success', result, 'Update Car Success', http.OK);
  };
  sendResponse(await postRequest(validatePayload));
};

const deleteCar = async (req, res) => {
  const { carId } = req.params;

  const postRequest = async () => commandHandler.deleteCar(carId);
  const sendResponse = async (result) => {
    /* eslint no-unused-expressions: [2, { allowTernary: true }] */
    result.err
      ? wrapper.response(
        res,
        'fail',
        result,
        'Delete Card Fail',
        httpError.NOT_FOUND,
      )
      : wrapper.response(res, 'success', result, 'Delete Car Success', http.OK);
  };
  sendResponse(await postRequest());
};

const getCar = async (req, res) => {
  const { carId } = req.params;

  const getData = async () => queryHandler.getCar(carId);
  const sendResponse = async (result) => {
    result.err
      ? wrapper.response(res, 'fail', result, 'Get Car Fail', httpError.NOT_FOUND)
      : wrapper.response(res, 'success', result, 'Get Car Success', http.OK);
  };
  sendResponse(await getData());
};

const getAllCar = async (req, res) => {
//   const { testId } = req.query;
  const getData = async () => queryHandler.getAllCar();
  const sendResponse = async (result) => {
    result.err
      ? wrapper.response(
        res,
        'fail',
        result,
        'Get All Car Fail',
        httpError.NOT_FOUND,
      )
      : wrapper.response(res, 'success', result, 'Get All Car Success', http.OK);
  };
  sendResponse(await getData());
};

module.exports = {
  createCar,
  updateCar,
  deleteCar,
  getCar,
  getAllCar
};
