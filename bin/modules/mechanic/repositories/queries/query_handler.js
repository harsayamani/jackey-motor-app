const Mechanic = require('./domain');
const Mysql = require('../../../../helpers/databases/mysql/db');
const config = require('../../../../infra/configs/global_config');

const db = new Mysql(config.get('/mysqlConfig'));
const invoice = new Mechanic(db);

const getInvoice = async (invoiceId) => {
  const getData = async () => {
    const result = await invoice.viewInvoice(invoiceId);
    return result;
  };
  const result = await getData();
  return result;
};

const getAllInvoice = async () => {
  const getData = async () => {
    const result = await invoice.viewAllInvoice();
    return result;
  };
  const result = await getData();
  return result;
};

module.exports = {
  getInvoice,
  getAllInvoice,
};
