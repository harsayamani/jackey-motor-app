const Query = require('./query');
const Customer = require('../../../customer/repositories/queries/query');
const Car = require('../../../car/repositories/queries/query');
const Item = require('../../../itemmechanic/repositories/queries/query');
const Service = require('../../../service/repositories/queries/query');
const wrapper = require('../../../../helpers/utils/wrapper');
const { NotFoundError, BadRequestError } = require('../../../../helpers/error');

class Invoice {

  constructor(db) {
    this.query = new Query(db);
    this.customer = new Customer(db);
    this.car = new Car(db);
    this.item = new Item(db);
    this.service = new Service(db);
  }

  async viewInvoice(invoiceId) {
    try {
      const invoice = await this.query.findInvoice('SELECT * FROM mechanic WHERE invoice_id=\''
        +invoiceId+ '\'');

      if (invoice.data.length < 1) {
        return wrapper.error(new NotFoundError('invoice not found'));
      }

      if (invoice.err) {
        return wrapper.error(new NotFoundError('there is an error query'));
      }

      const customer = await this.customer.findCustomer('SELECT * FROM customer WHERE cust_id=\''
        +invoice.data[0].cust_id+ '\'');

      if (customer.data.length < 1) {
        return wrapper.error(new NotFoundError('customer not found'));
      }

      if (customer.err) {
        return wrapper.error(new NotFoundError('there is an error query'));
      }

      const car = await this.car.findCar('SELECT * FROM car WHERE car_id=\''
        +invoice.data[0].car_id+ '\'');

      if (car.data.length < 1) {
        return wrapper.error(new NotFoundError('car not found'));
      }

      if (car.err) {
        return wrapper.error(new NotFoundError('there is an error query'));
      }

      let item = await this.item.findItem('SELECT * FROM itemmechanic WHERE invoice_id=\''
        +invoiceId+ '\'');

      if (item.data.length > 0) {
        const itemPromises = item.data.map((data) => {
          return new Promise((resolve, reject) => {
            (async () => {
              const service = await this.service.findService('SELECT * FROM service WHERE service_id=\''
              +data.service_id+ '\'');

              let result = {
                item_id: data.item_id,
                service_name: service.data[0].serv_supl,
                qty: data.qty,
                price: data.price,
                amount: data.amount
              };

              resolve(result);
            })();
          });
        });

        item.data = await Promise.all(itemPromises);
      }

      if (car.err) {
        return wrapper.error(new NotFoundError('there is an error query'));
      }

      const data = {
        invoice_id: invoice.data[0].invoice_id,
        customer: customer.data[0],
        car: car.data[0],
        chasis: invoice.data[0].chasis,
        received: invoice.data[0].received,
        reg_date: invoice.data[0].reg_date,
        date_in: invoice.data[0].date_in,
        date_out: invoice.data[0].date_out,
        engine: invoice.data[0].engine,
        km: invoice.data[0].km,
        mechanic: invoice.data[0].mechanic,
        item_list: item.data
      };

      return wrapper.data(data);
    } catch (error) {
      return wrapper.error(new BadRequestError(error));
    }
  }

  async viewAllInvoice() {
    try {
      const invoice = await this.query.findInvoice('SELECT * FROM mechanic');

      if (invoice.data.length < 1) {
        return wrapper.error(new NotFoundError('invoice not found'));
      }

      if (invoice.err) {
        return wrapper.error(new NotFoundError('there is an error query'));
      }

      return wrapper.data(invoice.data);
    } catch (error) {
      return wrapper.error(new BadRequestError(error));
    }
  }
}

module.exports = Invoice;
