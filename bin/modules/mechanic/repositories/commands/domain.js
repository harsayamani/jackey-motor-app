const Query = require('../queries/query');
const Customer = require('../../../customer/repositories/queries/query');
const Car = require('../../../car/repositories/queries/query');
const Command = require('./command');
const wrapper = require('../../../../helpers/utils/wrapper');
const randomstring = require('randomstring');
const { NotFoundError, BadRequestError } = require('../../../../helpers/error');

class Invoice {
  constructor(db) {
    this.command = new Command(db);
    this.query = new Query(db);
    this.customer = new Customer(db);
    this.car = new Car(db);
  }

  async insertInvoice(payload) {
    const { mechanic,car_id, cust_id, chasis, received, reg_date, date_in, date_out, engine, km } = payload;

    const customer = await this.customer.findCustomer('SELECT * FROM customer WHERE cust_id=\''
    +cust_id+ '\'');

    if (customer.data.length < 1) {
      return wrapper.error(new NotFoundError('customer not found'));
    }

    const car = await this.car.findCar('SELECT * FROM car WHERE car_id=\''
    +car_id+ '\'');

    if (car.data.length < 1) {
      return wrapper.error(new NotFoundError('car not found'));
    }

    const invoiceId = 'INV-'+randomstring.generate(12);

    let qy = 'INSERT INTO mechanic (invoice_id,car_id,cust_id,chasis,received,reg_date,date_in,date_out,engine,km,mechanic) ' +
      'VALUES (\''
      + invoiceId + '\',\''
      + car_id + '\',\''
      + cust_id + '\',\''
      + chasis + '\',\''
      + received + '\',\''
      + reg_date + '\',\''
      + date_in + '\',\''
      + date_out + '\',\''
      + engine + '\',\''
      + km + '\',\''
      + mechanic + '\')';

    const result = await this.command.insertInvoice(qy);

    if (result.err) {
      return wrapper.error(new BadRequestError(result.err));
    }

    const invoice  = await this.query.findInvoice('SELECT * FROM mechanic WHERE invoice_id=\''
    +invoiceId+ '\'');

    return wrapper.data(invoice.data);
  }

  async updateInvoice(invoiceId, payload) {
    let { mechanic,car_id, cust_id, chasis, received, reg_date, date_in, date_out, engine, km } = payload;

    const invoice = await this.query.findInvoice('SELECT * FROM mechanic WHERE invoice_id=\''
    +invoiceId+ '\'');

    if (invoice.data.length < 1) {
      return wrapper.error(new NotFoundError('invoice not found'));
    }

    !car_id? car_id = invoice.data[0].car_id : car_id;
    !cust_id? cust_id = invoice.data[0].cust_id : cust_id;
    !chasis? chasis = invoice.data[0].chasis : chasis;
    !received? received = invoice.data[0].received : received;
    !reg_date? reg_date = invoice.data[0].reg_date : reg_date;
    !date_in? date_in = invoice.data[0].date_in : date_in;
    !date_out? date_out = invoice.data[0].date_out : date_out;
    !engine? engine = invoice.data[0].engine : engine;
    !km? km = invoice.data[0].km : km;

    const customer = await this.customer.findCustomer('SELECT * FROM customer WHERE cust_id=\''
    +cust_id+ '\'');

    if (customer.data.length < 1) {
      return wrapper.error(new NotFoundError('customer not found'));
    }

    const car = await this.car.findCar('SELECT * FROM car WHERE car_id=\''
    +car_id+ '\'');

    if (car.data.length < 1) {
      return wrapper.error(new NotFoundError('car not found'));
    }

    let qy = 'UPDATE mechanic SET ' +
    'cust_id=\'' + cust_id + '\',' +
    'car_id=\'' + car_id + '\',' +
    'chasis=\'' + chasis + '\',' +
    'received=\'' + received + '\',' +
    'reg_date=\'' + reg_date + '\',' +
    'date_in=\'' + date_in + '\',' +
    'date_out=\'' + date_out + '\',' +
    'engine=\'' + engine + '\',' +
    'km=\'' + km + '\',' +
    'mechanic=\'' + mechanic + '\'' +
    'WHERE invoice_id=\'' + invoiceId + '\'' ;

    const result = await this.command.updateInvoice(qy);
    if (result.err) {
      return wrapper.error(new BadRequestError(result.err));
    }

    return wrapper.data(result.data);
  }

  async deleteInvoice(invoiceId) {
    const invoice = await this.query.findInvoice('SELECT * FROM mechanic WHERE invoice_id=\''
    +invoiceId+ '\'');

    if (invoice.data.length < 1) {
      return wrapper.error(new NotFoundError('invoice not found'));
    }

    let qy = 'DELETE FROM mechanic WHERE invoice_id=\''+invoiceId+'\'';

    const result = await this.command.deleteInvoice(qy);
    if (result.err) {
      return wrapper.error(new BadRequestError(result.err));
    }

    return wrapper.data(result.data);
  }
}

module.exports = Invoice;
