const Invoice = require('./domain');
const Mysql = require('../../../../helpers/databases/mysql/db');
const config = require('../../../../infra/configs/global_config');

const db = new Mysql(config.get('/mysqlConfig'));
const invoice = new Invoice(db);

const createInvoice = async (payload) => {
  const postCommand = async (payload) => invoice.insertInvoice(payload);
  return postCommand(payload);
};

const updateInvoice = async (invoiceId, payload) => {
  const postCommand = async (invoiceId, payload) => invoice.updateInvoice(invoiceId, payload);
  return postCommand(invoiceId, payload);
};

const deleteInvoice = async (invoiceId) => {
  const postCommand = async (invoiceId) => invoice.deleteInvoice(invoiceId);
  return postCommand(invoiceId);
};

module.exports = {
  createInvoice,
  updateInvoice,
  deleteInvoice
};
