const joi = require('joi');

const createInvoice = joi.object({
  cust_id: joi.string().required(),
  mechanic: joi.string().required(),
  car_id: joi.string().required(),
  chasis: joi.string().required(),
  received: joi.string().required(),
  reg_date: joi.string().required(),
  date_in: joi.string().required(),
  date_out: joi.string().required(),
  engine: joi.string().required(),
  km: joi.number().required(),
});

const updateInvoice= joi.object({
  cust_id: joi.string(),
  car_id: joi.string(),
  mechanic: joi.string(),
  chasis: joi.string(),
  received: joi.string(),
  reg_date: joi.string(),
  date_in: joi.string(),
  date_out: joi.string(),
  engine: joi.string(),
  km: joi.number(),
});

module.exports = {
  createInvoice,
  updateInvoice
};
